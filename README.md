# Senior Design Fall 2020

This is the repo for the 2019-2020 Smith Chart senior design project. The goal
is to make the large smith chart in the basement of Broun useable with an augmented
reality system.

## Members
* Ryan McGill
* Alex Kaylor
* Mandy Fields
* Will Broussard
* William Dovre
* Rebecca Thomas
* Phillip Champion